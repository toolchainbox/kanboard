# How to run Kanboard

## Debug Mode
```bash
podman run -it --rm -p 127.0.0.1:9000:80 -v $(pwd)/data:/var/www/html/data registry.gitlab.com/toolchainbox/kanboard:latest
```

## Server Mode
```bash
podman run --name kanboard -d -p 127.0.0.1:9000:80 -v $(pwd)/data:/var/www/html/data registry.gitlab.com/toolchainbox/kanboard:latest
```

Data is stored in "$(pwd)/data" foilder. 

Login via: admin / admin
