FROM php:8.2-apache

RUN apt-get update && apt-get install -y \
        libfreetype6-dev \
        libjpeg62-turbo-dev \
        libpng-dev \
    && docker-php-ext-install -j$(nproc) iconv \
    && docker-php-ext-install -j$(nproc) gd \
    && apt-get -y autoremove && apt-get clean \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/* \
    && curl -L -o /tmp/kanboard.tar.gz https://github.com/kanboard/kanboard/archive/refs/tags/v1.2.32.tar.gz \
    && tar -xzf /tmp/kanboard.tar.gz --strip 1 -C /var/www/html/ \
    && rm -Rf /tmp/kanboard.tar.gz
